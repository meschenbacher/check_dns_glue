package main

import (
	"fmt"
	"github.com/miekg/dns"
)

type Dig struct {
	// TODO
	// on unix, use ConfigClientReadFromFile function
	// on windows, https://medium.com/@justen.walker/breaking-all-the-rules-using-go-to-call-windows-api-2cbfd8c79724
	resolvers         []string
	default_resolvers []string
}

func (d *Dig) CurrentResolvers() []string {
	if d.resolvers != nil {
		return d.resolvers
	}
	return d.default_resolvers
}

func (d *Dig) ResetResolvers() {
	d.resolvers = nil
}

func (d *Dig) exchange(msg *dns.Msg) (*dns.Msg, error) {
	c := dns.Client{}
	err := fmt.Errorf("could not connect to nameserver %v with any protocol (tcp, udp)", d.CurrentResolvers())
	for _, resolver := range d.CurrentResolvers() {
		for _, proto := range []string{"udp", "tcp"} {
			c.Net = proto
			ret, _, err := c.Exchange(msg, resolver)
			if err == nil {
				return ret, nil
			}
		}
	}
	return nil, err
}

func (d *Dig) GetMsg(Type uint16, domain string) (*dns.Msg, error) {
	domain = dns.Fqdn(domain)
	msg := newMsg(Type, domain)
	return d.exchange(msg)
}

func (d *Dig) SetResolver(host string) {
	d.resolvers = []string{host}
}

func (d *Dig) SetResolvers(hosts []string) {
	d.resolvers = hosts
}

// func (d *Dig) ResetDNS() {
// d.resolver = ""
// }

func newMsg(Type uint16, domain string) *dns.Msg {
	domain = dns.Fqdn(domain)
	msg := new(dns.Msg)
	msg.Id = dns.Id()
	msg.RecursionDesired = true
	msg.Question = make([]dns.Question, 1)
	msg.Question[0] = dns.Question{
		Name:   domain,
		Qtype:  Type,
		Qclass: dns.ClassINET,
	}
	return msg
}

func (d *Dig) A(domain string) ([]*dns.A, error) {
	m := newMsg(dns.TypeA, domain)
	res, err := d.exchange(m)
	if err != nil {
		return nil, err
	}
	var As []*dns.A
	for _, v := range res.Answer {
		if a, ok := v.(*dns.A); ok {
			As = append(As, a)
		}
	}
	return As, nil
}

func (d *Dig) AAAA(domain string) ([]*dns.AAAA, error) {
	m := newMsg(dns.TypeAAAA, domain)
	res, err := d.exchange(m)
	if err != nil {
		return nil, err
	}
	var aaaa []*dns.AAAA
	for _, v := range res.Answer {
		if a, ok := v.(*dns.AAAA); ok {
			aaaa = append(aaaa, a)
		}
	}
	return aaaa, nil
}

func (d *Dig) NS(domain string) ([]*dns.NS, error) {
	m := newMsg(dns.TypeNS, domain)
	res, err := d.exchange(m)
	if err != nil {
		return nil, err
	}
	var Ns []*dns.NS
	for _, v := range res.Answer {
		if ns, ok := v.(*dns.NS); ok {
			Ns = append(Ns, ns)
		}
	}
	return Ns, nil
}

// query for all A and AAAA records (and convert them to .String()
func (d *Dig) AAAAA(domain string) ([]string, error) {
	ret := []string{}
	ip4, err := d.A(domain)
	if err != nil {
		return nil, err
	}
	for _, rr := range ip4 {
		ret = append(ret, rr.A.String())
	}
	ip6, err := d.AAAA(domain)
	if err != nil {
		return nil, err
	}
	for _, rr := range ip6 {
		ret = append(ret, rr.AAAA.String())
	}
	return ret, nil
}
