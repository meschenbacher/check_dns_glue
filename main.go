package main

import (
	"flag"
	"fmt"
	"github.com/NETWAYS/go-check"
	"github.com/NETWAYS/go-check/result"
	"github.com/miekg/dns"
	"os"
	"strings"
)

type CheckDnsGlueConfig struct {
	overall                             *result.Overall
	dig                                 *Dig
	zone                                string
	parentZone                          string
	nameserver                          string
	nameserverIps                       []string
	resolver                            string
	unreachableNameserverCriticality    int // check.{OK,Warning,Critical,Unknown}
	numParentNS, numUnreachableParentNS int
}

func checkNameserver(c *CheckDnsGlueConfig, ns *dns.NS, nsIp string) {

	if strings.Contains(nsIp, ":") {
		c.dig.SetResolver("[" + nsIp + "]:53")
	} else {
		c.dig.SetResolver(nsIp + ":53")
	}

	// found and foundExtra are nulled per NS
	found := make(map[string]bool)
	for _, ip := range c.nameserverIps {
		found[ip] = false
	}
	foundExtra := make([]string, 0)

	// we will query NS record but the authoritative zone nameserver will
	// return all nameservers and glues in the additional section
	msg, err := c.dig.GetMsg(dns.TypeNS, c.zone)
	if err != nil {
		c.numUnreachableParentNS++
		c.overall.Add(c.unreachableNameserverCriticality, fmt.Sprintf("the authoritative nameserver for zone %s %s [%s] could not be reached for NS retrieval", c.parentZone, ns.Ns, nsIp))
		if c.numUnreachableParentNS == c.numParentNS {
			c.overall.AddCritical(fmt.Sprintf("None of the authoritative nameservers for zone %s could not be reached for NS retrieval", c.parentZone))
		}
		return
	}
	if msg.Extra == nil {
		c.overall.AddCritical(fmt.Sprintf("the authoritative nameserver for zone %s %s [%s] does not contain an additional section", c.parentZone, ns.Ns, nsIp))
		return
	}
	for _, extra := range msg.Extra {
		if extra.Header().Name != c.nameserver {
			continue
		}

		var ip string

		switch extra.Header().Rrtype {
		case dns.TypeA:
			if t, ok := extra.(*dns.A); ok {
				ip = t.A.String()
			}
		case dns.TypeAAAA:
			if t, ok := extra.(*dns.AAAA); ok {
				ip = t.AAAA.String()
			}
		default:
			continue
		}

		if _, ok := found[ip]; !ok {
			foundExtra = append(foundExtra, ip)
		}
		for _, nsIp := range c.nameserverIps {
			if nsIp == ip {
				found[nsIp] = true
			}
		}
	}

	nameserverOK := true
	for _, ip := range c.nameserverIps {
		if !found[ip] {
			c.overall.AddCritical(fmt.Sprintf("the authoritative nameserver for zone %s %s [%s] does not contain the required glue ip address for nameserver %s: %s", c.parentZone, ns.Ns, nsIp, c.nameserver, ip))
			nameserverOK = false
		}
	}
	if len(foundExtra) > 0 {
		c.overall.AddWarning(fmt.Sprintf("the authoritative nameserver for zone %s %s [%s] contains additional ip address(es) for nameserver %s: %s", c.parentZone, ns.Ns, nsIp, c.nameserver, foundExtra))
		nameserverOK = false
	}
	if nameserverOK {
		c.overall.AddOK(fmt.Sprintf("the authoritative nameserver for zone %s %s [%s] contains exactly the listed ip address(es) for nameserver %s", c.parentZone, ns.Ns, nsIp, c.nameserver))
	}
}

func main() {
	defer check.CatchPanic()

	var unreachable string
	c := &CheckDnsGlueConfig{}
	flag.StringVar(&c.resolver, "resolver", "one.one.one.one:53", "Set the recursive resolver")
	flag.StringVar(&unreachable, "unreachable", "unknown", "Set the error level of unreachable nameservers (ok, critical, warning or unknown)")
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Fprintf(flag.CommandLine.Output(), "  ZONE\n\tDNS zone with trailing dot. Example: example.org.\n")
		fmt.Fprintf(flag.CommandLine.Output(), "  NAMESERVER\n\tFQDN for the nameserver which should be checked for glue records with trailing dot.\n")
		fmt.Fprintf(flag.CommandLine.Output(), "  IP...\n\tOne or more IP addresses which must be present exactly\n")
		os.Exit(3)
	}

	flag.Parse()
	args := flag.Args()

	if len(args) < 3 {
		flag.Usage()
	}
	switch unreachable {
	case "ok":
		c.unreachableNameserverCriticality = check.OK
	case "warning":
		c.unreachableNameserverCriticality = check.Warning
	case "critical":
		c.unreachableNameserverCriticality = check.Critical
	case "unknown":
		c.unreachableNameserverCriticality = check.Unknown
	default:
		flag.Usage()
	}

	zone := args[0]
	if !strings.HasSuffix(zone, ".") {
		zone = zone + "."
	}
	parentZone := strings.Join(strings.Split(zone, ".")[1:], ".")
	if !strings.HasSuffix(parentZone, ".") {
		parentZone = parentZone + "."
	}
	nameserver := args[1]
	if !strings.HasSuffix(nameserver, ".") {
		nameserver = nameserver + "."
	}
	ips := args[2:]

	overall := result.Overall{}
	dig := Dig{}

	c.overall = &overall
	c.dig = &dig
	c.zone = zone
	c.parentZone = parentZone
	c.nameserver = nameserver
	c.nameserverIps = ips

	checkDnsGlue(c)
	check.Exit(overall.GetStatus(), overall.GetOutput())
}

func checkDnsGlue(c *CheckDnsGlueConfig) {
	c.dig.SetResolver(c.resolver)
	parentNs, err := c.dig.NS(c.parentZone)
	if err != nil {
		check.Exit(3, err.Error())
	}
	c.numParentNS = len(parentNs)

	for _, ns := range parentNs {
		c.dig.SetResolver(c.resolver)
		nsIps, err := c.dig.AAAAA(ns.Ns)
		if err != nil {
			c.overall.Add(c.unreachableNameserverCriticality, fmt.Sprintf("failure retrieving A and/or AAAA: %v", err))
			continue
		}

		for _, ip := range nsIps {
			checkNameserver(c, ns, ip)
		}
	}
}
