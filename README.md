# `check_dns_glue`

`check_dns_glue` is a tool (initially created as icinga2 check plugin) which can
be used to make sure that the *parent zone* for *zone* contains exactly the
required DNS glue records for a nameserver and all its IP addresses. It does so
by querying all authoritative nameservers for *parent zone* on all available IP
addresses. *Parent zone* is inferred from *zone* by stripping one level.

From an infrastructure point of view, glue records are set seldomly and
rarely change, let alone suddenly and uncommanded.

Use it for the extra piece of mind (e.g. when switching between registrars).

example usage
```
./check_dns_glue<TAB> example.org ns1.example.org 169.254.1.1 2001:db8::53
```

`check_dns_glue` requires a resolver to be set (default `one.one.one.one`).

# build

The tool can be built on both windows and linux:

```
go build
```

# deploy

example `commands.conf`

```
object CheckCommand "dns glue" {
	import "plugin-check-command"
	command = [ "/usr/local/bin/check_dns_glue" ]
	arguments = {}
	if ("$dnsglue_resolver$") {
		arguments += {
			"-resolver" = {
				value = "$dnsglue_resolver$"
			}
		}
	}
	if ("$dnsglue_unreachable$") {
		arguments += {
			"-unreachable" = {
				value = "$dnsglue_unreachable$"
			}
		}
	}
	arguments += {
		"0" = {
			skip_key = true
			value = "$dnsglue_zone$"
			required = true
		}
		"1" = {
			skip_key = true
			value = "$dnsglue_nameserver$"
			required = true
		}
		"2" = {
			skip_key = true
			value = "$dnsglue_ips$"
			repeat_key = true
			required = true
		}
	}
}
```

example `services.conf`

```
apply Service "ns1.example.org glue records" {
	import "generic-service"
	check_command = "dns glue"
	//vars.dnsglue_resolver = "9.9.9.9:53"
	//vars.dnsglue_unreachable = "warning"
	vars.dnsglue_zone = "example.org"
	vars.dnsglue_nameserver = "ns1.example.org"
	vars.dnsglue_ips = [ "2001:db8::53", "169.254.1.1" ]
	assign where host.name == "icinga-master.yourdomain.com"
}
```
