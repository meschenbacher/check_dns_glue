// +build dev

package main

// copy from here https://medium.com/@justen.walker/breaking-all-the-rules-using-go-to-call-windows-api-2cbfd8c79724

import (
	"internal/syscall/windows"
	"os"
	"syscall"
	"unsafe"
)

func GetDefaultResolvers() ([]string, error) {
	// copied from go/src/net/interface_windows.go
	var b []byte
	resolvers := make([]string, 0)
	l := uint32(15000) // recommended initial size
	for {
		b = make([]byte, l)
		err := windows.GetAdaptersAddresses(syscall.AF_UNSPEC, windows.GAA_FLAG_INCLUDE_PREFIX, 0, (*windows.IpAdapterAddresses)(unsafe.Pointer(&b[0])), &l)
		if err == nil {
			if l == 0 {
				return nil, nil
			}
			break
		}
		if err.(syscall.Errno) != syscall.ERROR_BUFFER_OVERFLOW {
			return nil, os.NewSyscallError("getadaptersaddresses", err)
		}
		if l <= uint32(len(b)) {
			return nil, os.NewSyscallError("getadaptersaddresses", err)
		}
	}
	var aas []*windows.IpAdapterAddresses
	for aa := (*windows.IpAdapterAddresses)(unsafe.Pointer(&b[0])); aa != nil; aa = aa.Next {
		aas = append(aas, aa)
	}

	// see also go/src/internal/syscall/windows/syscall_windows.go
	for _, aa := range aas {
		for dsa := aa.FirstDnsServerAddress; dsa != nil; dsa = dsa.Next {
			fmt.Printf("%v\n", dsa.Address)
			resolvers = append(resolvers, dsa.Address)
		}
	}
	return resolvers, nil
}
