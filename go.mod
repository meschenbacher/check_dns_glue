module gitlab.com/meschenbacher/check_dns_glue

go 1.15

require (
	github.com/NETWAYS/go-check v0.0.0-20201207101832-89c3bfada341
	github.com/miekg/dns v1.1.0
	golang.org/x/crypto v0.0.0-20201124201722-c8d3bf9c5392 // indirect
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.0.0-20201204225414-ed752295db88 // indirect
)
